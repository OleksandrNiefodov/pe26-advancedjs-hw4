'use strict'
/*
Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

- це технологія, що використовується в JavaScript для асинхронного обміну даними з веб-сервером. Це означає, що JavaScript може отримувати дані з сервера без перезавантаження сторінки, роблячи інтерфейс більш динамічним.
AJAX не блокує інтерфейс користувача, Користувачі можуть продовжувати взаємодіяти з веб-сторінкою, поки дані завантажуються.  AJAX може значно пришвидшити завантаження веб-сторінок, оскільки завантажуються лише необхідні дані.

Завдання
Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

Технічні вимоги:
Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.
*/

const URL = 'https://ajax.test-danit.com/api/swapi/films';


fetch(URL)
    .then(res => res.json())
    .then(data => {

        console.log(data);

        const episodesList = document.createElement('div');

        document.body.append(episodesList);

        data.forEach(({ episodeId, name, openingCrawl, characters }) => {

            const episodeContainer = document.createElement('div');

            episodesList.append(episodeContainer);

            episodeContainer.insertAdjacentHTML('beforeend', `
            <div>
                <h2>Episode ${episodeId}: ${name}</h2>
                <p>Characters list: </p>
                <p>${openingCrawl}</p>    
            </div>`);

            const charactersNames = episodeContainer.querySelector('p');

            characters.forEach((elem) => {
                fetch(elem)
                    .then(res => res.json())
                    .then((elem) => {
                        charactersNames.textContent += `${elem.name},`;
                    })
            });
        })
    })
    .catch((error) => {
        console.log(error)
    })

